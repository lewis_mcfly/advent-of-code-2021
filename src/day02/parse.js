const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    const { groups: { command, value } } = /^(?<command>[a-z]*) (?<value>\d*)$/.exec(line) 
    return { command, value: parseInt(value) }
  })

  return parsed
}

module.exports = parse
