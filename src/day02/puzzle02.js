const parse = require('./parse')

const commands = {
  FORWARD: 'forward',
  UP: 'up',
  DOWN: 'down'
}

const solve = inputPath => {
  const parsed = parse(inputPath)
  const status = { horizontal: 0, depth: 0, aim: 0 }
  for (const instruction of parsed) {
    switch (instruction.command) {
    case commands.FORWARD:
      status.horizontal += instruction.value
      status.depth += status.aim * instruction.value
      break
    case commands.UP:
      status.aim -= instruction.value
      break
    case commands.DOWN:
      status.aim += instruction.value
      break
    }
  }
  return status.depth * status.horizontal
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
