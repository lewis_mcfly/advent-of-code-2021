const parse = require('./parse')

const commands = {
  FORWARD: 'forward',
  UP: 'up',
  DOWN: 'down'
}

const solve = inputPath => {
  const parsed = parse(inputPath)
  const position = { horizontal: 0, depth: 0 }
  for (const instruction of parsed) {
    switch (instruction.command) {
    case commands.FORWARD:
      position.horizontal += instruction.value
      break
    case commands.UP:
      position.depth -= instruction.value
      break
    case commands.DOWN:
      position.depth += instruction.value
      break
    }
  }
  return position.depth * position.horizontal
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
