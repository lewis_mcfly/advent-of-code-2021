const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    return parseInt(line)
  })

  return parsed
}

module.exports = parse
