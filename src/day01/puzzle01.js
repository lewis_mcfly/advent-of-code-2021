const parse = require('./parse')

const solve = inputPath => {
  const parsed = parse(inputPath)
  return parsed.reduce((previous, current, index) => current > parsed[index - 1] ? previous + 1 : previous, 0)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
