const parse = require('./parse')

const solve = inputPath => {
  const parsed = parse(inputPath)
  let count = 0
  let currentValue = parsed[0] + parsed[1] + parsed[2]
  for (let n = 3; n < parsed.length; n += 1) {
    const nextValue = parsed[n - 2] + parsed[n - 1] + parsed[n]
    if (nextValue > currentValue) {
      count += 1
    }
    currentValue = nextValue
  }
  return count
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
