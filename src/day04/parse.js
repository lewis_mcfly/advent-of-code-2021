const { readlines, chunkify } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath, false)

  return {
    numbers: parsed[0].split(',').map(x => parseInt(x)),
    boards: chunkify(parsed.slice(1), line => line.length <= 1)
      .filter(chunk => chunk.length > 0)
      .map(board => board.map(line => line.split(/\s+/).map(x => parseInt(x))))
  }
}

module.exports = parse
