const parse = require('./parse')
const { every, sum } = require('../utils')

const solve = inputPath => {
  const { numbers, boards } = parse(inputPath)

  const playBoard = (board, number) => {
    for (let y = 0; y < board.length; y += 1) {
      for (let x = 0; x < board[y].length; x += 1) {
        if (board[y][x] === number) {
          board[y][x] = null
          if (every(board[y], item => item === null) || every(board.map(line => line[x]), item => item === null)) {
            return true
          }
        }
      }
    }
    return false
  }

  for (const number of numbers) {
    for (const board of boards) {
      const bingo = playBoard(board, number)
      if (bingo) {
        return number * sum(board.map(line => sum(line)))
      }
    }
  }

  throw new Error('No bingo')
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
