const { 
  readlines,
  objectAndArrayFunction,
  every,
  forIn,
  filterObject,
  slugify,
  cleanDuplicates,
  sum,
  min,
  max,
  intersection,
  binToDec,
  chunkify
} = require('../utils')

test('readlines', () => {
  expect(readlines(`${__dirname}/__data__/input.txt`)).toEqual(['a b c', 'a', '123'])
})

test('objectAndArrayFunction', () => {
  const commonFunction = objectAndArrayFunction(() => 'object', () => 'array')
  expect(commonFunction({})).toEqual('object')
  expect(commonFunction([])).toEqual('array')

  const commonFunctionWithArgument = objectAndArrayFunction(o => Object.keys(o), a => a.length)
  expect(commonFunctionWithArgument({ foo: 'foo', bar: 'bar' })).toEqual(['foo', 'bar'])
  expect(commonFunctionWithArgument(['foo', 'bar'])).toEqual(2)
})

test('forIn', () => {
  const mockFn = jest.fn()
  forIn({ foo: 'bar', bar: 'baz'}, (key, value) => mockFn(key, value))
  expect(mockFn).toHaveBeenNthCalledWith(1, 'foo', 'bar')
  expect(mockFn).toHaveBeenNthCalledWith(2, 'bar', 'baz')
})

test('every', () => {
  expect(every([1, 2, 3], value => value > 0)).toEqual(true)
  expect(every([1, 2, 3], value => value < 0)).toEqual(false)
  expect(every([1, 2, 3], value => value > 1)).toEqual(false)

  expect(every({ foo: 1, bar: 2, baz: 3}, value => value > 0)).toEqual(true)
  expect(every({ foo: 1, bar: 2, baz: 3}, value => value < 0)).toEqual(false)
  expect(every({ foo: 1, bar: 2, baz: 3}, value => value > 1)).toEqual(false)
})

test('filterObject', () => {
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (key, value) => value > 0 && key !== 'baz')).toEqual({ foo: 1, bar: 2})
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (_, value) => value < 0)).toEqual({})
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (key, value) => value > 1 && key)).toEqual({ bar: 2, baz: 3})
})

test('slugify', () => {
  expect(slugify('Mdr le slu & gify    ')).toEqual('mdr-le-slu-and-gify')
})

test('cleanDuplicates', () => {
  expect(cleanDuplicates([1, 2, 1, 2, 3])).toEqual([1, 2, 3])
})

test('sum', () => {
  expect(sum([1, 2, 3])).toEqual(6)
})

test('min', () => {
  expect(min([1, 2, 3])).toEqual(1)
})

test('max', () => {
  expect(max([1, 2, 3])).toEqual(3)
})

test('intersection', () => {
  expect(intersection([1, 2, 3, 4], [1, 2, 4, 5])).toEqual([1, 2, 4])
  expect(intersection([1, 2, 3, 4], [5, 6, 7, 8])).toEqual([])
  expect(intersection([1, 2, 3, 4])).toEqual([1, 2, 3, 4])
  expect(intersection([1, 2, 3, 4], [1, 2, 4, 5], [2, 3, 4, 5])).toEqual([2, 4])
  expect(intersection([1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12])).toEqual([])
})


test('binToDec', () => {
  expect(binToDec([1, 0, 1, 1, 0])).toEqual(22)
})


test('chunkify', () => {
  expect(chunkify([1, null, 3, 4], null)).toEqual([[1], [3, 4]])
  expect(chunkify(['foo', 'bar', '--------', 'baz'], element => element.length > 3)).toEqual([['foo', 'bar'], ['baz']])
})
