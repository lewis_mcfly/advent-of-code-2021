const fs = require('fs')

const readlines = (path, filterEmptyLines = true) => fs
  .readFileSync(path, 'utf-8').split('\n')
  .map(line => line.trim())
  .filter(line => filterEmptyLines === false || line.length > 0)


const objectAndArrayFunction = (objectBehaviour, arrayBehaviour) => {
  return object => {
    if (Array.isArray(object)) {
      return arrayBehaviour(object)
    } else {
      return objectBehaviour(object)
    }
  }
}

const forIn = (o, f) => {
  for (const key of Object.keys(o)) {
    f(key, o[key])
  }
}

const every = (collection, predicate) => {
  return objectAndArrayFunction(
    object => {
      for (const key of Object.keys(object)) {
        if (!predicate(object[key])) {
          return false
        }
      }
      return true
    },
    array => {
      for (const element of array) {
        if (!predicate(element)) {
          return false
        }
      }
      return true
    }
  )(collection)
}


const filterObject = (o, predicate) => {
  const filtered = {}
  forIn(o, (key, value) => {
    if (predicate(key, value)) {
      filtered[key] = value
    }
  })
  return filtered
}

const slugify = text => text.toString().toLowerCase().trim()
  .replace(/&/g, '-and-')         // Replace & with 'and'
  .replace(/[\s\W-]+/g, '-') 

const cleanDuplicates = array => {
  if (array.length === 0) return array
  const cleaned = [array[0]]
  for (const element of array.slice(1)) {
    if (!cleaned.includes(element)) {
      cleaned.push(element)
    }
  }
  return cleaned
}

const sum = array => array.reduce((a, b) => a + b)
const min = array => array.reduce((a, b) => b < a ? b : a)
const max = array => array.reduce((a, b) => b > a ? b : a)

const intersection = (...arrays) => {
  if (arrays.length === 1) {
    return arrays[0]
  } else if (arrays.length === 2) {
    return arrays[0].filter(element => arrays[1].includes(element))
  } else {
    const [first, second, ...others] = arrays
    return intersection(intersection(first, second), ...others)
  }
}

const binToDec = binary => binary
  .reverse()
  .reduce((previous, current, index) => previous + (current ? Math.pow(2, index) : 0), 0)

const isFunction = (functionToCheck) => functionToCheck && {}.toString.call(functionToCheck) === '[object Function]'

const chunkify = (collection, separator) => {
  const chunks = []
  let chunk = []
  const checkElement = element => isFunction(separator) ? separator(element) : element === separator
  for (const element of collection) {
    if (checkElement(element)) {
      chunks.push([...chunk])
      chunk = []
    } else {
      chunk.push(element)
    }
  }
  if (chunk.length > 0) {
    chunks.push([...chunk])
  }
  return chunks
}
  
module.exports = {
  readlines,
  objectAndArrayFunction,
  forIn,
  every,
  filterObject,
  slugify,
  cleanDuplicates,
  sum,
  min,
  max,
  intersection,
  binToDec,
  chunkify
}
