const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    return line.split('').map(digit => parseInt(digit))
  })

  return parsed
}

module.exports = parse
