const { binToDec, sum } = require('../utils')
const parse = require('./parse')

const rating = (binaries, currentIndex, most = true) => {
  if (binaries.length === 1) {
    return binaries[0]
  } else {
    const mean = sum(binaries.map(binary => binary[currentIndex])) / binaries.length
    const criteria = mean >= 0.5 ? (most ? 1 : 0) : (most ? 0 : 1)
    return rating(
      binaries.filter(binary => binary[currentIndex] === (most ? criteria : criteria)),
      currentIndex + 1,
      most
    )
  }
}

const solve = inputPath => {
  const binaries = parse(inputPath)
  const oxygen = rating(binaries, 0, true)
  const co2 = rating(binaries, 0, false)

  return binToDec(oxygen) * binToDec(co2)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
