const { binToDec } = require('../utils')
const parse = require('./parse')

const solve = inputPath => {
  const parsed = parse(inputPath)
  const gamma = parsed[0].map((_, index) => parsed.reduce((previous, current) => previous + current[index] , 0) > parsed.length / 2 ? 1 : 0)
  const epsilon = gamma.map(value => value ? 0 : 1)
  return binToDec(gamma) * binToDec(epsilon)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
