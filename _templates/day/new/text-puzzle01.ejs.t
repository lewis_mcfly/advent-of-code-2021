---
to: src/day<%= index %>/__test__/puzzle01.test.js
---
const solve = require('../puzzle01')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual('solution')
})