[![pipeline status](https://gitlab.com/lewis_mcfly/advent-of-code-2021/badges/master/pipeline.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2021/commits/master)
[![coverage report](https://gitlab.com/lewis_mcfly/advent-of-code-2021/badges/master/coverage.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2021/commits/master)

# Advent of Code

## 2021

[https://adventofcode.com/2021](https://adventofcode.com/2021)
